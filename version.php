<?php
/**
 * Copyright (c) 2020 SSAT (The Schools Network) Ltd.
 * 
 * This software is released under the MIT License.
 * https://opensource.org/licenses/MIT
 */

/**
 * SSAT config.
 *
 * @package   theme_ssat
 * @copyright 2019-2020 SSAT (The Schools Network) Ltd.
 * @license   https://opensource.org/licenses/MIT
 */


// This line protects the file from being accessed by a URL directly.
defined('MOODLE_INTERNAL') || die();

// This is the version of the plugin.
$plugin->version = '2019062400';

// This is the version of Moodle this plugin requires.
$plugin->requires = '2019052000.05';

// This is the component name of the plugin - it always starts with 'theme_'
// for themes and should be the same as the name of the folder.
$plugin->component = 'theme_ssat';

// This is a list of plugins, this plugin depends on (and their versions).
$plugin->dependencies = [
    'theme_boost' => '2019052000'
];



