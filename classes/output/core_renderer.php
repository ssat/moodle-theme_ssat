<?php
/**
 * Copyright (c) 2020 SSAT (The Schools Network) Ltd.
 * 
 * This software is released under the MIT License.
 * https://opensource.org/licenses/MIT
 */


namespace theme_ssat\output;

use moodle_url;

defined('MOODLE_INTERNAL') || die;

/**
 * SSAT theme renderer overrides
 *
 * @package    theme_ssat
 * @copyright  2019 SSAT, www.ssatuk.co.uk
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

class core_renderer extends \core_renderer {

    /**
     * Renders the login form.
     *
     * @param \core_auth\output\login $form The renderable.
     * @return string
     */
    public function render_login(\core_auth\output\login $form) {
        //return parent::render_login($form);

        global $CFG, $SITE;

        $context = $form->export_for_template($this);

        // Override because rendering is not supported in template yet.
        if ($CFG->rememberusername == 0) {
            $context->cookieshelpiconformatted = $this->help_icon('cookiesenabledonlysession');
        } else {
            $context->cookieshelpiconformatted = $this->help_icon('cookiesenabled');
        }
        $context->errorformatted = $this->error_text($context->error);
        $url = $this->get_logo_url();
        if ($url) {
            $url = $url->out(false);
        }
        $context->logourl = $url;
        $context->sitename = format_string($SITE->fullname, true,
                ['context' => \context_course::instance(SITEID), "escape" => false]);

        $context->bypass = optional_param('ssat_bypass_sso', false, PARAM_BOOL);

        //return $this->render_from_template('core/loginform', $context);
        return $this->render_from_template('theme_ssat/loginform', $context);
    }
}
