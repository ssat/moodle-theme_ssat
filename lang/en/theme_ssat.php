<?php
/**
 * Copyright (c) 2020 SSAT (The Schools Network) Ltd.
 * 
 * This software is released under the MIT License.
 * https://opensource.org/licenses/MIT
 */



 // This line protects the file from being accessed by a URL directly.
defined('MOODLE_INTERNAL') || die();

// A description shown in the admin theme selector.
$string['choosereadme'] = 'Theme SSAT is a child theme of Boost.';
// The name of our plugin.
$string['pluginname'] = 'SSAT';
// We need to include a lang string for each block region.
$string['region-side-pre'] = 'Right';
