<?php
/**
 * Copyright (c) 2020 SSAT (The Schools Network) Ltd.
 * 
 * This software is released under the MIT License.
 * https://opensource.org/licenses/MIT
 */



// This line protects the file from being accessed by a URL directly.                                                               
defined('MOODLE_INTERNAL') || die();
 
// We will add callbacks here as we add features to our theme.
